r = 0.132
velLim = 9998
yG = 0.3765

wheels = {
{
    type = "Fixed",
    radius = r,
    id = 1,
    x = 0.5053,
    y = -yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 2,
    x = 0.5053,
    y = yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 3,
    x = -0.0511,
    y = -yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 4,
    x = -0.0511,
    y = yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 5,
    x = -0.5438,
    y = -yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 6,
    x = -0.5438,
    y = yG,
    maxAngVel = velLim
}

}
