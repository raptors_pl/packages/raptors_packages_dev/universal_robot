#include "exampleNoetic.hpp"

exampleNoetic::exampleNoetic(std::optional<std::map<unsigned int, ROAC::config_t>> config, std::string name) : Robot(config, name)
{
    robot_pub_odom = nh_.advertise<nav_msgs::Odometry>(name + std::string("/odom"), 10);

    robot_sub = nh_.subscribe<geometry_msgs::TwistStamped>(name + std::string("cmd_vel"), 1, &exampleNoetic::cmdVelCallback, this);
    robot_pub = nh_.advertise<universal_robot_msg::VelocityWheel>(name + std::string("velocity"), 1, true);
    robot_sub_wheels = nh_.subscribe<universal_robot_msg::VelocityWheel>(name + std::string("wheel_feedback"), 1, &exampleNoetic::wheelVelCallback,
            this);  // wheel_feedback

    serviceOdom = nh_.advertiseService(name + std::string("set_robot_pose"), &exampleNoetic::serviceOdomCallback, this);
    serviceOdomHome = nh_.advertiseService(name + std::string("set_home_pose"), &exampleNoetic::serviceOdomHomeCallback, this);
    robot_sub_initialpose =
            nh_.subscribe<geometry_msgs::PoseWithCovarianceStamped>(name + std::string("initialpose"), 1, &exampleNoetic::initialPoseCallback, this);
}

exampleNoetic::~exampleNoetic() { }

bool exampleNoetic::serviceOdomCallback(universal_robot_msg::setRobotPose::Request& req, universal_robot_msg::setRobotPose::Response& res)
{
    tf::Quaternion const quat(req.odometry.orientation.x, req.odometry.orientation.y, req.odometry.orientation.z, req.odometry.orientation.w);

    this->setPose({req.odometry.position.x, req.odometry.position.y, tf::getYaw(quat)});

    return true;
}

bool exampleNoetic::serviceOdomHomeCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
{
    this->resetPose();
    return true;
}

void exampleNoetic::initialPoseCallback(geometry_msgs::PoseWithCovarianceStamped::ConstPtr const& pose)
{  // for rviz
    tf::Quaternion const quat(pose->pose.pose.orientation.x, pose->pose.pose.orientation.y, pose->pose.pose.orientation.z, pose->pose.pose.orientation.w);

    this->setPose({pose->pose.pose.position.x, pose->pose.pose.position.y, tf::getYaw(quat)});
}

void exampleNoetic::cmdVelCallback(geometry_msgs::TwistStamped::ConstPtr const& cmd)
{
    auto _wheelData = (this->reciveCMD({cmd->twist.linear.x, cmd->twist.linear.y, cmd->twist.angular.z}));

    universal_robot_msg::motor _motor;
    universal_robot_msg::VelocityWheel _motors;
    _motors.values.reserve(_wheelData.size());

    double _margin(1);

    for (auto& _w : _wheelData)
    {
        _motor.ID = _w.first;
        _motor.AngVelocity = _w.second[0];
        _motor.steerAngle = _w.second[1];

        if (_w.second[2] < _margin)
            _margin = _w.second[2];

        _motors.values.emplace_back(_motor);
    }
    for (auto& _msg : _motors.values)
    {
        _msg.AngVelocity *= _margin;
    }
    robot_pub.publish(_motors);
}

void exampleNoetic::publishOdomerty(std::array<double, 6> odom)
{
    ros::Time l_m_current_time = ros::Time::now();
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(odom[2]);
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = l_m_current_time;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_footprint";

    odom_trans.transform.translation.x = odom[0];

    odom_trans.transform.translation.y = odom[1];
    odom_trans.transform.translation.z = 0;
    odom_trans.transform.rotation = odom_quat;
    odom_broadcaster.sendTransform(odom_trans);

    /****** odometry messege*/
    nav_msgs::Odometry msg;
    msg.header.stamp = l_m_current_time;
    msg.header.frame_id = "odom";

    msg.pose.pose.position.x = odom[0];
    msg.pose.pose.position.y = odom[1];
    msg.pose.pose.position.z = 0;
    msg.pose.pose.orientation = odom_quat;
    msg.child_frame_id = "base_footprint";
    msg.twist.twist.linear.x = odom[3];
    msg.twist.twist.linear.y = odom[4];
    msg.twist.twist.angular.z = odom[5];
    robot_pub_odom.publish(msg);
}

void exampleNoetic::wheelVelCallback(universal_robot_msg::VelocityWheel::ConstPtr const& vel)
{
    std::map<int, std::array<double, 2>> _data;
    ros::Time _time;
    _time = ros::Time::now();

    for (auto& v : vel->values)
    {
        _data.insert(std::make_pair<int, std::array<double, 2>>((int)v.ID, {(double)v.AngVelocity, (double)v.steerAngle}));
    }
    this->publishOdomerty(this->reciveWheelData(_data, std::make_pair<int, unsigned int>(int(_time.sec), (unsigned int)(_time.nsec))));
}
