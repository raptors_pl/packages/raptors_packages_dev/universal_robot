#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <tf/transform_broadcaster.h>
#include "ROAC.hpp"

#include "universal_robot_msg/motor.h"
#include "universal_robot_msg/setRobotPose.h"
#include "universal_robot_msg/VelocityWheel.h"

class exampleNoetic : public ROAC::Robot
{
private:
    tf::TransformBroadcaster odom_broadcaster;  // tf odom
    ros::Publisher robot_pub_odom;

    ros::NodeHandle nh_;
    ros::Subscriber robot_sub;  // (1) cmd -> geometry_msgs::TwistStamped
    ros::Publisher robot_pub;  // (2) wheel -> universal_robot_msg::VelocityWheel
    ros::Subscriber robot_sub_wheels;  // (3) wheel -> universal_robot_msg::VelocityWheel

    void cmdVelCallback(geometry_msgs::TwistStamped::ConstPtr const& cmd);  // (1)
    void wheelVelCallback(universal_robot_msg::VelocityWheel::ConstPtr const& vel);  // (3)
    ros::ServiceServer serviceOdom;  // zmiana położenia oraz orientacji robota
    bool serviceOdomCallback(universal_robot_msg::setRobotPose::Request& req, universal_robot_msg::setRobotPose::Response& res);
    ros::ServiceServer serviceOdomHome;  // zmiana położenia oraz orientacji robota
    bool serviceOdomHomeCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);
    ros::Subscriber robot_sub_initialpose;  // for rviz
    void initialPoseCallback(geometry_msgs::PoseWithCovarianceStamped::ConstPtr const& pose);
    void publishOdomerty(std::array<double, 6>);

public:
    exampleNoetic(std::optional<std::map<unsigned int, ROAC::config_t>>, std::string name);
    ~exampleNoetic();
};
