#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
namespace CN
{
/**
 * @brief reads fields from config.conf in form of dictionary of strings 
 * 
 */
class readConfig
{
    std::string m_path;
    std::fstream m_File;
    std::string m_fullName; // = _path + std::string("config.conf");
    std::string m_delimiter1 = "<-";
    char m_delimiter2 = '@';
    std::size_t m_poseD1;
    std::size_t m_poseD2;

public:
    readConfig() = delete;
    readConfig(std::string path_);
    ~readConfig();
    std::map<std::string, std::string> LoadParameters();
};
}