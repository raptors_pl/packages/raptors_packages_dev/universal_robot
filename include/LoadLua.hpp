#include "Config.hpp"
#include <lua.hpp>
#include <map>
#include "readConfig.hpp"
namespace ROAC
{
    class LoadLua
    {
        const char* m_path;
        lua_State* m_lua;
        char m_errorCode;
        

        void setError(char e){m_errorCode = e;};
        char readFixed(std::pair<unsigned int, ROAC::config_t>& _p);
        char readSteer(std::pair<unsigned int, ROAC::config_t>& _p);
        std::string errormsg(char m);

    public:
        LoadLua(std::string);
        LoadLua(std::string, bool);
        LoadLua() = delete;
        ~LoadLua();
        std::optional<std::map<unsigned int, ROAC::config_t>> readFile();
        
    };
}