#include <memory>
#include <map>
#include <vector>
#include "Wheels.hpp"
#include "Config.hpp"

namespace KIN
{
    class Kinematics
    {
    protected:
        std::map<int, std::unique_ptr<wheel> > m_wheels;
    public:
        Kinematics();
        virtual ~Kinematics();
        virtual std::array<double,3> forwardKinematics(std::map<int, std::array<double,2>>&) = 0;
        virtual std::map<int, std::array<double, 3>> inverseKinematics(std::array<double, 3>) = 0;
        virtual void addWheel(int, const ROAC::config_t&) = 0;
    };
    class AdvanceKinematics : public Kinematics
    {
        unsigned int m_wheelCount{0};
        std::vector<std::pair<double, double> > m_Xvel;
        std::vector<std::pair<double, double> > m_Yvel;
        std::vector<int> m_id;
        bool m_validateCurvature;
    public:
        AdvanceKinematics();
        ~AdvanceKinematics();
        std::array<double,3> forwardKinematics(std::map<int, std::array<double,2>>&) override;
        std::map<int, std::array<double, 3>> inverseKinematics(std::array<double, 3>) override;
        double leastSqMethod(std::vector<std::pair<double,double>>, double& error);
        double leastSqMethod(std::vector<std::pair<double,double>>);
        void addWheel(int, const ROAC::config_t&) override;
    };
    
}