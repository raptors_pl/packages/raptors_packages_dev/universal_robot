#include "Config.hpp"
#include "readConfig.hpp"
#include "yaml-cpp/yaml.h"

namespace ROAC
{
    class LoadYaml
    {
        std::string m_path;
        YAML::Node m_yaml;
        char m_errorCode;

        void setError(char e){m_errorCode = e;};
        char readFixed(std::pair<unsigned int, ROAC::config_t>& _p, YAML::detail::iterator_value mapa);
        char readSteer(std::pair<unsigned int, ROAC::config_t>& _p, YAML::detail::iterator_value mapa);
        std::string errormsg(char m){return std::to_string(m);};

    public:
        LoadYaml(std::string);
        LoadYaml(std::string, bool);
        LoadYaml() = delete;
        ~LoadYaml();
        std::optional<std::map<unsigned int, ROAC::config_t>> readFile();
        
    };
}