#include <vector>
#include <array>

namespace ROAC
{
    class Robot;
}
namespace KIN
{  
    class wheel
    {
    protected:
        double m_radius;
        struct kin1{
            double X;
            double Y;
            kin1(double _x, double _y){X = _x; Y = _y;}
        }m_pose;
        double m_maxAngularVelocity;
        int m_CCW;

    public:
        wheel(double rad, double x, double y, double maxAnV, bool ccw);
        virtual ~wheel();
        virtual std::array<double,3> inverseKinematics(std::array<double,3>) = 0; 
        virtual std::array<double,2> getComponents(double vel_, double steerAngle_) = 0;
        virtual double XfromCente() = 0; //{ return m_poseXY[0]; };
        virtual double YfromCente() = 0;// { return m_poseXY[1]; };
        virtual double BaseAngularVelocity(std::array<double,2>) = 0;
        virtual bool ValidateCurvature(std::array<double,3> _V) = 0;
    };
    class wheelF : public wheel
    {
    private:
        double m_virtualR;
    public:
        wheelF(double R, double x, double y, double maxAnV, bool ccw);
        ~wheelF();
        std::array<double,3> inverseKinematics(std::array<double,3>) override;
        std::array<double,2> getComponents(double AnVel_, double steerAngle_) override;
        double YfromCente() override {return m_virtualR;};
        double XfromCente() override {return 0;};
        double BaseAngularVelocity(std::array<double,2> _Vxy) override {return -(_Vxy[0] / m_virtualR); };
        bool ValidateCurvature(std::array<double,3> _V) override {return (_V[1] == 0) ;};
    };
    class wheelS : public wheel
    {
        double m_poseXY_Squared_Sum;
        double m_ValidatedsteerAngle; 
        struct{
            double Min;
            double Max;
        }m_steerAngle;
        int m_CCW2; // if velocity vector is rotated 180deg 
    public:
        wheelS(double R, double x, double y, double maxAnV, bool ccw, double _minAng, double _maxAng);
        ~wheelS();
        std::array<double,3> inverseKinematics(std::array<double,3>) override;
        std::array<double,2> getComponents(double AnVel_, double steerAngle_) override;
        double YfromCente() override {return m_pose.X;};
        double XfromCente() override {return m_pose.Y;};
        // teoretycznie poniższe może być uproszczone do postaci jak w wheelF jeżeli zmienimy baze kanoniczną, ale czy warto dodawać macierz 2x2?
        double BaseAngularVelocity(std::array<double,2> _Vxy) override {return (_Vxy[1]*m_pose.X - _Vxy[0]*m_pose.Y) / (m_poseXY_Squared_Sum);};
        bool ValidateCurvature(std::array<double,3> _V) override;
    };

}