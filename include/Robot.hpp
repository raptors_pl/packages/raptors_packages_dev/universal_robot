#include "Config.hpp"
#include <map>
#include <optional>
#include <string>
#include <vector>
#include <memory>

namespace KIN
{
    class Kinematics;
}
namespace ROAC
{
    class Odometry;
    class Robot{
        std::string m_name;
        std::unique_ptr<Odometry> m_odometria;
        std::unique_ptr<KIN::Kinematics> m_kinematics;

    public:
        Robot(std::optional<std::map<unsigned int,config_t>>, std::string name);
        Robot(std::optional<std::map<unsigned int,config_t>>); // <id><wheel_config>
        Robot() = delete;
        ~Robot();
        std::array<double,6> getOdom();
        void setPose(std::array<double,3>);
        void resetPose();
        std::map<int,std::array<double,3> > reciveCMD(std::array<double,3>);
        std::array<double,6> reciveWheelData(std::map<int, std::array<double, 2>>, std::pair<int, unsigned int> _time);
        std::string getName();

    };
    
}
