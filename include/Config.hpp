#pragma once
#include <optional>

namespace ROAC{
    struct config_t{
        enum type{Fixed = 1, Steer = 2};
        type T;
        double radius, x, y, maxAngVel;
        std::optional<bool> ccw;
        std::optional<double> minsteerAngle, maxsteerAngle;
        
    };
}