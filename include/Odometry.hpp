#include <map>

namespace ROAC{
    class Odometry
    {
        std::optional<double> m_currentTime;
        double m_previousTime;
        struct odom1{
            double poseX;
            double poseY;
            double orientation;
            odom1& operator=(std::array<double,3> const& obj){
                poseX = obj[0];
                poseY = obj[1];
                orientation = obj[2];
                return *this;
            }
            odom1(double _x,double _y, double _z){poseX = _x; poseY = _y; orientation = _z;}
        }odomPose;
        struct odom2{
            double linearX;
            double linearY;
            double angularZ;
            odom2& operator=(std::array<double,3> const& obj){
                linearX = obj[0];
                linearY = obj[1];
                angularZ = obj[2];
                return *this;
            }
            odom2(double _x,double _y, double _z){linearX = _x; linearY = _y; angularZ = _z;}
        }odomVel;
    public:
        Odometry();
        ~Odometry();
        void update(std::array<double,3>, std::pair<int,unsigned int> _time);
        std::array<double, 3> getPose();
        void setPose(std::array<double, 3> _p);
        std::array<double, 3> getVelocity();
        void Zero();
        double time(){return m_currentTime.value_or(0);};
    };
    
    
}