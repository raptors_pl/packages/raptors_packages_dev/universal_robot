r = 0.09
velLim = 9998
xG = 4.0
yG = 2.0

wheels = {{
    type = "Fixed",
    radius = r,
    id = 1,
    x = xG,
    y = -yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 2,
    x = xG,
    y = yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 3,
    x = -xG,
    y = yG,
    maxAngVel = velLim
}, {
    type = "Fixed",
    radius = r,
    id = 4,
    x = -xG,
    y = -yG,
    maxAngVel = velLim
}}
