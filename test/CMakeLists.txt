cmake_minimum_required(VERSION 3.18)
project(api_test)

add_compile_options(-std=c++17)
# add_compile_options(-Os)

link_directories(
    ${CMAKE_SOURCE_DIR}/../../lib/
)

include_directories(
    ${CMAKE_SOURCE_DIR}/../../lib/include
    # ${CMAKE_SOURCE_DIR}/../../ROAC/include
    ${CMAKE_SOURCE_DIR}/../../lib/lua-5.4.2/include
)

add_executable(${PROJECT_NAME} test.cpp)
target_link_libraries(${PROJECT_NAME}  ROAC yaml lua)