#include <iostream>
#include <Robot.hpp>
#include <Config.hpp>
#include <LoadLua.hpp>
#define _DEBUG(x) std::cout << "\x1B[32m" << x << "\033[0m" << std::endl;

int main(int argc, char **argv)
{

    ROAC::LoadLua Reader("../config.lua");
    auto _config = Reader.readFile();
    ROAC::Robot robocik(_config);
    std::pair<int, unsigned  int> _time{0,0};
    for (int i(0); i < 10; i++)
    {
        std::map<int, std::array<double, 2>> _dataWheels;
        auto _data = robocik.reciveCMD({0.19, 0.0, 0.0});
        std::cout << "\t"
                  << "Zadana: "
                  << "0.19 | 0 | 0" << std::endl;
        for (auto &_m : _data)
        {
            std::cout << "\t"
                      << "ID: " << _m.first << " | vel:  " << _m.second[0] << " \t" << _m.second[1] << "\t" << _m.second[2] << std::endl;
            _dataWheels.insert(std::make_pair(_m.first, std::array<double, 2>({_m.second[0], _m.second[1]})));
        }
        std::cout << "***\n";
        auto _dataOdom = robocik.reciveWheelData(_dataWheels, _time);
        std::cout << "\t" << _dataOdom[0] << " | " << _dataOdom[1] << " | " << _dataOdom[2] << " | " << _dataOdom[3] << " | " << _dataOdom[4] << " | " << _dataOdom[5] << std::endl;
        _time.second += 12135472;
    }
    return 0;
}