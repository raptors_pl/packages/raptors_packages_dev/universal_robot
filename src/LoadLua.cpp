#include "LoadLua.hpp"
#include <iostream>
#include <cmath>
#define DEBUG_INFO 
//#define _DEBUG(x) std::cout <<  "\x1B[33m" << x << "\033[0m"<< std::endl;
#define _DEBUG(x) 
#define ERROR_0 "ok"
#define ERROR_1 "given path is not valid"
#define ERROR_2 "table 'wheels' doesn't exist"
#define ERROR_3 "field 'id'"
#define ERROR_4 "field 'radius'"
#define ERROR_5 "field 'x'"
#define ERROR_6 "field 'y'"
#define ERROR_7 "field 'maxAngVel'"
#define ERROR_8 "field 'maxSterAng'"
#define ERROR_9 "field 'minSterAng'"

ROAC::LoadLua::LoadLua(std::string _file, bool _CN)
    : m_path(_file.c_str()), m_lua(luaL_newstate()), m_errorCode(0)
{
    if (_CN)
    {
        CN::readConfig seconLayer(_file);
        std::map<std::string, std::string> data = seconLayer.LoadParameters();
        m_path = (data["path"] + "/" + data["name"] + "." + data["type"] ).c_str();
    }
    if (luaL_dofile(m_lua, m_path) != LUA_OK)
        setError(1);
}
ROAC::LoadLua::LoadLua(std::string _file) : ROAC::LoadLua::LoadLua(_file, false)
{}

ROAC::LoadLua::~LoadLua()
{
    std::cout << errormsg(m_errorCode) << std::endl;
    lua_close(m_lua);
}
std::optional<std::map<unsigned int, ROAC::config_t>> ROAC::LoadLua::readFile()
{
    if (m_errorCode){
        std::cout << errormsg(m_errorCode) << std::endl;
        return {};
    }
    std::map<unsigned int, ROAC::config_t> kinStruct;
    lua_getglobal(m_lua, "wheels");
    // if on top of the stack is table type then push nil on top of that representing key of table -> there are two elements on stack wheels has idx=1
    lua_isnil(m_lua, -1) ? setError(2) : lua_istable(m_lua, -1) ? lua_pushnil(m_lua) : setError(2);
    // take consecutively pairs key-value from table if there is nothing left then 'lua_next' returns 0 and push nothig to the stack
    while(lua_next(m_lua, 1))
    {
        std::pair<unsigned int, ROAC::config_t> singleWheel;

        if (lua_istable(m_lua, -1)) lua_pushstring(m_lua, "type"); else {lua_pop(m_lua, 1); continue;}
        lua_gettable(m_lua, 3);
        DEBUG_INFO std::cout << "Reading wheel ... " << std::endl;

        if(lua_tostring(m_lua, -1) == std::string("Fixed")) {
            _DEBUG("Fixed here")
            lua_pop(m_lua, 1); 
            m_errorCode = readFixed(singleWheel);
        }
        else if (lua_tostring(m_lua, -1) == std::string("Steer")) {lua_pop(m_lua, 1); m_errorCode = readSteer(singleWheel);}
        else 
        {
            DEBUG_INFO std::cout << "Bad type " << std::endl;
            lua_pop(m_lua, 2); // delete value of subTable and subTable
            continue;
        }
        DEBUG_INFO std::cout << errormsg(m_errorCode) << std::endl;
        kinStruct.insert(singleWheel);


        lua_pop(m_lua, 2); // delete value of subTabe and subTable
    }
    if (kinStruct.size() == 0)
    {
        DEBUG_INFO std::cout << "Empty configuration " << std::endl;
        lua_settop(m_lua, 0);
        return {};
    }
    else
    {
        DEBUG_INFO std::cout << "Not Empty configuration " << std::endl;
        _DEBUG(kinStruct.size())
        lua_settop(m_lua, 0);
        return kinStruct;
    }
    lua_settop(m_lua, 0);
    return {};
}
// on top of the stack is subTable for 1Dof wheel
char ROAC::LoadLua::readFixed(std::pair<unsigned int, ROAC::config_t>& _p)
{
    _p.second.T = ROAC::config_t::Fixed;
    
    lua_getfield(m_lua, -1, "id"); // push on top of the stack value with key 'id'
    if (lua_isinteger(m_lua, -1)) // check if on top of the stack is proper type, if 'id' not exist then on top is nil
        _p.first = abs(lua_tointeger(m_lua, -1));
    else
        return 3;
    
    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "radius");
    if (lua_isnumber(m_lua, -1))
        _p.second.radius = lua_tonumber(m_lua, -1);
    else
        return 4;


    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "x");
    if (lua_isnumber(m_lua, -1))
        _p.second.x = lua_tonumber(m_lua, -1);
    else
        return 5;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "y");
    if (lua_isnumber(m_lua, -1))
        _p.second.y = lua_tonumber(m_lua, -1);
    else
        return 6;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "maxAngVel");
    if (lua_isnumber(m_lua, -1))
        _p.second.maxAngVel =  std::fabs( lua_tonumber(m_lua, -1) );
    else
        return 7;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "ccw");
    if (lua_isboolean(m_lua, -1))
        _p.second.ccw = lua_tonumber(m_lua, -1);
    else
        _p.second.ccw = true;

    return 0;
}
// on top of the stack is subTable for 2Dof wheel
char ROAC::LoadLua::readSteer(std::pair<unsigned int, ROAC::config_t>& _p)
{
    _p.second.T = ROAC::config_t::Steer;

    lua_getfield(m_lua, -1, "id");
    if (lua_isinteger(m_lua, -1))
        _p.first = abs(lua_tointeger(m_lua, -1));
    else
        return 3;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "radius");
    if (lua_isnumber(m_lua, -1))
        _p.second.radius = lua_tonumber(m_lua, -1);
    else
        return 4;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "x");
    if (lua_isnumber(m_lua, -1))
        _p.second.x = lua_tonumber(m_lua, -1);
    else
        return 5;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "y");
    if (lua_isnumber(m_lua, -1))
        _p.second.y = lua_tonumber(m_lua, -1);
    else
        return 6;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "maxAngVel");
    if (lua_isnumber(m_lua, -1))
        _p.second.maxAngVel = std::fabs( lua_tonumber(m_lua, -1) );
    else
        return 7;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "maxSterAng");
    if (lua_isnumber(m_lua, -1))
        _p.second.maxsteerAngle = lua_tonumber(m_lua, -1);
    else
        return 8;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "minSterAng");
    if (lua_isnumber(m_lua, -1))
        _p.second.minsteerAngle = lua_tonumber(m_lua, -1);
    else
        return 9;

    lua_pop(m_lua, 1);
    lua_getfield(m_lua, -1, "ccw");
    if (lua_isboolean(m_lua, -1))
        _p.second.ccw = lua_tonumber(m_lua, -1);
    else
        _p.second.ccw = true;

    return 0;
}
std::string ROAC::LoadLua::errormsg(char m)
{
    switch (m)
    {
    case 0: return std::string(ERROR_0);
    case 1: return std::string(ERROR_1);
    case 2: return std::string(ERROR_2);
    case 3: return std::string(ERROR_3);
    case 4: return std::string(ERROR_4);
    case 5: return std::string(ERROR_5);
    case 6: return std::string(ERROR_6);
    case 7: return std::string(ERROR_7);
    case 8: return std::string(ERROR_8);
    case 9: return std::string(ERROR_9);
    default: return std::string("no such code\n");
    }
    return std::string();
}