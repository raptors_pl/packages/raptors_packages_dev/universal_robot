#include "exampleNoetic.hpp"
#include <ros/package.h>

int main(int argc, char** argv)
{
    std::string this_pkg_path = ros::package::getPath("universal_robot") + std::string("/config");
    ros::init(argc, argv, "universal_robot_node");

    ROAC::LoadLua _Config(this_pkg_path + std::string("/exm.lua"));
    auto _params = _Config.readFile();

    if (!_params.has_value())
    {
        printf("No configuration detected\n");
        std::exit(1);
    }
    
    exampleNoetic Robocik(_params, "");

    ros::spin();
    std::cout << "End\n";
    return 0;
}