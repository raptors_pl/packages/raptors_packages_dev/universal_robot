#include "Wheels.hpp"
#include <cmath>
#include <numeric>
namespace KIN
{
    wheel::~wheel(){};
    wheel::wheel(double rad, double x, double y, double maxAnV, bool ccw)
        : m_radius(rad), m_pose(x, y), m_maxAngularVelocity(maxAnV)
        {
           if ( (ccw && y >= 0) ||  (!ccw && y < 0)) m_CCW = 1;
           else if ((ccw && y < 0) || (!ccw && y >= 0)) m_CCW = -1;
        };

    wheelF::~wheelF(){}
    wheelF::wheelF(double R, double x, double y, double maxAnV, bool ccw)
        : wheel(R, x, y, maxAnV, ccw)
    {
        m_virtualR = (std::pow(x, 2) + std::pow(y, 2)) / y;
    };
    std::array<double, 3> wheelF::inverseKinematics(std::array<double, 3> _cmd)
    {
        std::array<double, 3> _return{(  (_cmd[0] - (_cmd[2] * m_virtualR))  / m_radius)  *m_CCW, 0, 0};
        _return[0] == 0 ? _return[2] = m_maxAngularVelocity : _return[2] = (m_maxAngularVelocity / std::fabs(_return[0]) );

        return _return;
    }
    std::array<double, 2> wheelF::getComponents(double AnVel_, double steerAngle_)
    {
        return std::array<double, 2>{AnVel_*m_radius*m_CCW , 0};
    }

    wheelS::~wheelS(){}
    wheelS::wheelS(double R, double x, double y, double maxAnV, bool ccw, double _minAng, double _maxAng)
        : wheel(R, x, y, maxAnV, ccw), m_steerAngle({_minAng, _maxAng})
        , m_poseXY_Squared_Sum(std::pow(x,2) + std::pow(y,2))
        , m_CCW2(m_CCW)
        {};

    std::array<double,3> wheelS::inverseKinematics(std::array<double,3> _cmd)
    { 
        std::array<double,3> _return{0, 0, 0};
        double _vx = _cmd[0] - _cmd[2] * m_pose.Y;
        double _vy = _cmd[1] + _cmd[2] * m_pose.X;  
        _return[0] =  m_CCW2 * (std::sqrt(std::pow(_vx, 2) + std::pow(_vy,2)) / m_radius);
        // _return[1] = std::atan2(_vy, _vx); angle is calculated by calling ValdiateCurvature
        _return[1] = m_ValidatedsteerAngle;
        _return[0] == 0 ? _return[2] = m_maxAngularVelocity : _return[2] = (m_maxAngularVelocity / std::fabs( _return[0]));
        
        return _return;
    };
    std::array<double,2> wheelS::getComponents(double AnVel_, double steerAngle_)
    { // steerAngle_ in radians
        return std::array<double,2>{(AnVel_ * m_radius) * std::cos(steerAngle_)*m_CCW
                                    , (AnVel_ * m_radius) * std::sin(steerAngle_)*m_CCW};
    }
    bool wheelS::ValidateCurvature(std::array<double,3> _V)
    {
        m_CCW2 = m_CCW;
        if( !(std::accumulate(_V.begin(), _V.end(), 0.0)) ) // if velocity is 0 then do not change steer angle
            return true;
        else
            m_ValidatedsteerAngle = std::atan2((_V[2]*m_pose.X) +_V[1], (-_V[2]*m_pose.Y)+_V[0] );
        
        if (m_steerAngle.Min > m_ValidatedsteerAngle || m_ValidatedsteerAngle > m_steerAngle.Max){
            m_ValidatedsteerAngle = std::atan2( -((_V[2]*m_pose.X) +_V[1]), -((-_V[2]*m_pose.Y)+_V[0]) ); // try other direction - rotate 180
            m_CCW2 = -m_CCW2; // change motor spin direction due to rotation of velocity vector
            if (m_steerAngle.Min > m_ValidatedsteerAngle || m_ValidatedsteerAngle > m_steerAngle.Max) 
                return false;
            return true;
        }
        else
            return true;
    }
}
