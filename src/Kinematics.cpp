#include "Kinematics.hpp"
#include <cmath>
#include <iostream> //delete later
namespace KIN
{
    Kinematics::Kinematics(){};
    Kinematics::~Kinematics(){};
    AdvanceKinematics::AdvanceKinematics(){};
    AdvanceKinematics::~AdvanceKinematics(){};
    std::array<double,3> AdvanceKinematics::forwardKinematics(std::map<int, std::array<double,2>>& _wheelData)
    {
        
        std::array<double,2> _comp;
        for (auto& _m : _wheelData)
        {
            m_id.emplace_back(_m.first);
            _comp = m_wheels[_m.first] -> getComponents(_m.second[0], _m.second[1]);
            m_Xvel.emplace_back(std::make_pair( m_wheels[_m.first] -> YfromCente(), _comp[0] ));
            m_Yvel.emplace_back(std::make_pair( m_wheels[_m.first] -> XfromCente(), _comp[1] ));
        }
        
        _comp[0] = leastSqMethod(m_Xvel);
        _comp[1] = leastSqMethod(m_Yvel);
        

        double _omega{0};
        int _i{0};
        for (auto& _v : m_id)
        {
            _omega += m_wheels[_v] -> BaseAngularVelocity({m_Xvel[_i].second - _comp[0], m_Yvel[_i].second - _comp[1]});
            _i++;
        }
        _omega = _omega/_i;

        m_Xvel.clear();
        m_Yvel.clear();
        m_id.clear();
        
        return std::array<double,3>{_comp[0], _comp[1], _omega};
    }
    std::map<int, std::array<double, 3>> AdvanceKinematics::inverseKinematics(std::array<double, 3> _cmd)
    {
        std::map<int, std::array<double, 3>> _return;
        for (auto& _p: m_wheels)
        {
            m_validateCurvature = _p.second -> ValidateCurvature(_cmd);
            if (!m_validateCurvature) printf("Unable to set desired steer angle for wheel with id: %d\n", _p.first);

            _return.insert(std::make_pair(_p.first, _p.second -> inverseKinematics(_cmd)));
        }
        return _return;
    } 
    double AdvanceKinematics::leastSqMethod(std::vector<std::pair<double,double>> _set, double& error)
    {
        int n = _set.size();
        double sum_xy(0), sum_x(0), sum_y(0), sum_x2(0);
        for (auto& v : _set)
        {
            sum_xy += v.first * v.second;
            sum_x += v.first;
            sum_y += v.second;
            sum_x2 += std::pow(v.first, 2);
        }
        if (sum_x == 0 && sum_x2 == 0) return sum_y;

        double m = ((n * sum_xy) - (sum_x * sum_y)) / ((n * sum_x2) - (sum_x * sum_x));
        double b = (sum_y - (m * sum_x)) / n;
        std::array<double, 2> _e{0};
        for (auto& v: _set)
        {
            _e[0] = std::fabs(v.second - (m*v.first + b));
            if (_e[0] > _e[1]) _e[1] = _e[0];
        }
        return b;
    }
    double AdvanceKinematics::leastSqMethod(std::vector<std::pair<double,double>> _set)
    {
        int n = _set.size();
        double sum_xy(0), sum_x(0), sum_y(0), sum_x2(0);
        for (auto v : _set)
        {
            sum_xy += v.first * v.second;
            sum_x += v.first;
            sum_y += v.second;
            sum_x2 += std::pow(v.first, 2);
        }
        if (sum_x == 0 && sum_x2 == 0) return sum_y / n;
        double m = ((n * sum_xy) - (sum_x * sum_y)) / ((n * sum_x2) - (sum_x * sum_x));
        return (sum_y - (m * sum_x)) / n;
    }
    void AdvanceKinematics::addWheel(int _id, const ROAC::config_t& _c)
    {
        if (_c.T == 1)
            m_wheels.insert(std::make_pair(_id, std::make_unique<wheelF>(_c.radius,  _c.x, _c.y, _c.maxAngVel, _c.ccw.value())));
        else if (_c.T == 2)
            m_wheels.insert(std::make_pair(_id, std::make_unique<wheelS>(_c.radius,  _c.x, _c.y, _c.maxAngVel, _c.ccw.value(), _c.minsteerAngle.value(), _c.maxsteerAngle.value())));
        else
            return;
        m_wheelCount++;
        m_Xvel.reserve(m_wheelCount);
        m_Yvel.reserve(m_wheelCount);
        m_id.reserve(m_wheelCount);
    }
}