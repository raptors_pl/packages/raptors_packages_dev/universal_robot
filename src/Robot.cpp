#include "Robot.hpp"
#include "Kinematics.hpp"
#include "Odometry.hpp"
#include <iostream>
//#define _DEBUG(x) std::cout <<  "\x1B[32m" << x << "\033[0m"<< std::endl;
#define _DEBUG(x)
namespace ROAC
{
    Robot::Robot(std::optional<std::map<unsigned int, config_t>> _p) : ROAC::Robot::Robot(_p, std::string()){};
    Robot::Robot(std::optional<std::map<unsigned int, config_t>> _p, std::string name)
        : m_name(name),
          m_kinematics(std::make_unique<KIN::AdvanceKinematics>()),
          m_odometria(std::make_unique<Odometry>())
    {
        
        if(!_p.has_value()){
            printf("Configuartion Error\n");
            std::exit(1);
        }
        for (const auto &mapa : _p.value())
            m_kinematics->addWheel(mapa.first, mapa.second);
        
    }
    Robot::~Robot(){};
    std::array<double, 6> Robot::getOdom()
    {
        std::array<double, 6> result;
        std::array<double, 3> ar1 = m_odometria -> getPose();
        std::array<double, 3> ar2 = m_odometria -> getVelocity();
        std::copy (ar1.cbegin(), ar1.cend(), result.begin());
        std::copy (ar2.cbegin(), ar2.cend(), result.begin() + 3);
        return result;
    }
    void Robot::setPose(std::array<double, 3> _p)
    {
        m_odometria -> setPose(_p);
    }
    void Robot::resetPose()
    {
        m_odometria -> setPose(std::array<double,3>{0});
    }
    std::map<int, std::array<double, 3>> Robot::reciveCMD(std::array<double, 3> _cmd)
    {
        return m_kinematics -> inverseKinematics(_cmd);
    }
    std::array<double, 6> Robot::reciveWheelData(std::map<int, std::array<double, 2>> _data, std::pair<int, unsigned int> _time)
    {
        auto _dataK = m_kinematics -> forwardKinematics(_data);
        m_odometria -> update(_dataK, _time);
        return  this -> getOdom();
    }
    std::string Robot::getName()
    {
        return m_name;
    }
}