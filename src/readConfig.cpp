#include "readConfig.hpp"

using namespace CN;

readConfig::readConfig(std::string path_)
    : m_path(path_)
{
    if(path_.empty())
        m_fullName = "config.conf";
    else if (*(path_.end()) == '/')
        m_fullName = path_ + "config.conf";
    else
        m_fullName = path_ + '/' + "config.conf";
    
    m_File.open(m_fullName, std::ios::in);
    if(!m_File.is_open())
    {
        std::cout << "file not opened\n";
        std::exit(1);
    }
   
}
readConfig::~readConfig()
{
    m_File.close();
}
std::map<std::string, std::string> readConfig::LoadParameters()
{
    std::string line; 
    std::string subVar;
    std::string subVal;
    std::map<std::string, std::string> data;

    while(getline(m_File, line))
    {
        if(line.empty())
        {
            // std::cout << "empty linie\n";
            continue;
        }
        

        if (*(line.begin()) == '$')
        {
            m_poseD2 = line.find(m_delimiter2);
            if(m_poseD2 != std::string::npos)
                line = line.substr(0, m_poseD2);

            m_poseD1 = line.find(m_delimiter1);
            if(m_poseD1 != std::string::npos)
            {
                subVar = line.substr(1, m_poseD1 - 1);
                subVal = line.substr(m_poseD1+m_delimiter1.size(), line.size());
            }
            else
                continue;
            
            subVar.erase(std::remove_if(subVar.begin(), subVar.end(), isspace), subVar.end());
            subVal.erase(std::remove_if(subVal.begin(), subVal.end(), isspace), subVal.end());

            // std::cout << subVar << "_" << subVal << '\n';
        }
        else
            continue;

        if( !( subVar.empty() || subVal.empty() ))
            data.insert(std::make_pair(subVar, subVal)); //wpisane wartosci do slownika
    }
    if(data.find("path") == data.end()) // jezeli scierzka do pliku nie zostala podana
        data.insert(std::make_pair<std::string, std::string>("path", "default"));

    else if (*(data["path"].end()) == '/')
    {
        data["path"] = data["path"].substr(0, data["path"].size()-1);
    }

    if(data.find("type") == data.end()) // jezeli typ pliku nie zostal podany
        data.insert(std::make_pair<std::string, std::string>("type", "lua"));
    if(data.find("name") == data.end()) // jezeli nazwa pliku nie zostala podana
        data.insert(std::make_pair<std::string, std::string>("name", "default"));

    return data;

}