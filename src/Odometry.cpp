#include "Odometry.hpp"
#include  <cmath>
#include <iostream>
//#define _DEBUG(x) std::cout <<  "\x1B[32m" << x << "\033[0m"<< std::endl;
#define _DEBUG(x)
namespace ROAC
{
    Odometry::Odometry()
        : odomPose(0,0,0), odomVel(0,0,0)
    {};
    Odometry::~Odometry(){};
    void Odometry::update(std::array<double,3> _vel, std::pair<int,unsigned int> _time)
    {
        
        m_previousTime = m_currentTime.value_or(_time.first+(_time.second*1e-9));
        m_currentTime = _time.first+(_time.second * 1e-9);
        double _dt = m_currentTime.value() - m_previousTime;
        odomPose.poseX += ((odomVel.linearX * std::cos(odomPose.orientation)) - (odomVel.linearY * std::sin(odomPose.orientation))) * _dt;
        odomPose.poseY += ((odomVel.linearX * std::sin(odomPose.orientation)) + (odomVel.linearY * std::cos(odomPose.orientation))) * _dt;
        odomPose.orientation += (_vel[2]) * _dt;

        odomVel.linearX  = _vel[0];
        odomVel.linearY  = _vel[1];
        odomVel.angularZ = _vel[2];
        
    }
    std::array<double, 3> Odometry::getPose()
    {
        _DEBUG("Odometry getPose: " << odomPose.poseX << " _*_ " <<  odomPose.poseY << " _*_ " << odomPose.orientation)
        return {odomPose.poseX, odomPose.poseY, odomPose.orientation};
    }
    void Odometry::setPose(std::array<double, 3> _p) 
    {
        odomPose = _p;
    }
    std::array<double, 3> Odometry::getVelocity()
    {
        return {odomVel.linearX, odomVel.linearY, odomVel.angularZ};
    }
    void Odometry::Zero()
    {
        m_currentTime.reset();
        m_previousTime = 0;
        odomPose = (odom1){0,0,0};
        odomVel = (odom2){0,0,0};
    }
}