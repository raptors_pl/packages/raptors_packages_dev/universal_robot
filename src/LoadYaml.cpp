#include "LoadYaml.hpp"
#define DEBUG_INFO 

template <typename T>
auto LoadParam = [](std::string param, const YAML::detail::iterator_value &config)
{
    if (config[param])
        return config[param].as<T>();
    else
        std::cout << "\033[1;33m"
                  << "Warn: "
                  << "\033[0m"
                  << "parameter " << param << " doesn't exist\n";
    return T();
};
ROAC::LoadYaml::LoadYaml(std::string _file, bool _CN)
    : m_path(_file)
{
    if (_CN)
    {
        CN::readConfig seconLayer(_file);
        std::map<std::string, std::string> data = seconLayer.LoadParameters();
        m_path = data["path"] + "/" + data["name"] + "." + data["type"] ;
    }

    try
    {
        m_yaml = YAML::LoadFile(m_path);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        setError(1);
    }
}
ROAC::LoadYaml::LoadYaml(std::string _file) : ROAC::LoadYaml::LoadYaml(_file, false)
{}
ROAC::LoadYaml::~LoadYaml()
{
    std::cout << m_errorCode << std::endl;
}
std::optional<std::map<unsigned int, ROAC::config_t>> ROAC::LoadYaml::readFile()
{
    std::map<unsigned int, ROAC::config_t> kinStruct;
    std::pair<unsigned int, ROAC::config_t> singleWheel;
    std::string fix_s = "Fixed"; 
    std::string ster_s = "Steer";

    if (!m_yaml["wheels"].IsDefined())
        setError(2);
    else{
        for (const auto &mapa : m_yaml["wheels"])
        {   
            if (mapa["type"].as<std::string>() == fix_s)
                m_errorCode = readFixed(singleWheel, mapa);
            else if (mapa["type"].as<std::string>() == ster_s)
                m_errorCode = readSteer(singleWheel, mapa);
            else continue;
            DEBUG_INFO std::cout << errormsg(m_errorCode) << std::endl;
            kinStruct.insert(singleWheel);
        }
    }
   
    if (kinStruct.size() == 0)
        return {};
    else
        return kinStruct;
}
char ROAC::LoadYaml::readFixed(std::pair<unsigned int, ROAC::config_t>& _p, YAML::detail::iterator_value mapa)
{
    _p.second.T = ROAC::config_t::Fixed;
    _p.first = LoadParam<int>("id", mapa);
    _p.second.radius = LoadParam<double>("radius", mapa);
    _p.second.x = LoadParam<double>("x", mapa);
    _p.second.y = LoadParam<double>("y", mapa);
    _p.second.maxAngVel = LoadParam<double>("maxAngVel", mapa);
    _p.second.ccw = LoadParam<bool>("CCW", mapa);
    return 0;
}
char ROAC::LoadYaml::readSteer(std::pair<unsigned int, ROAC::config_t>& _p, YAML::detail::iterator_value mapa)
{
    _p.second.T = ROAC::config_t::Steer;
    _p.first = LoadParam<int>("id", mapa);
    _p.second.radius = LoadParam<double>("radius", mapa);
    _p.second.x = LoadParam<double>("x", mapa);
    _p.second.y = LoadParam<double>("y", mapa);
    _p.second.maxAngVel = LoadParam<double>("maxAngVel", mapa);
    _p.second.ccw = LoadParam<bool>("CCW", mapa);
    _p.second.minsteerAngle = LoadParam<double>("minSterAng", mapa);
    _p.second.maxsteerAngle = LoadParam<double>("maxSterAng", mapa);
    return 0;
}